/*
 * Bucket sort algorithm
 */
#include<bits/stdc++.h>
using namespace std;
const int N = 20;

int main() {
    int cases, n;
    cin >> cases;
    while(cases--){
        cin >> n; //read the size of array
        int table[N+1];
        memset(table, 0, sizeof table);

        int num;
        while(n--){
            cin >> num;
            table[num] = 1;
        }

        cin >> n; //read the target number
        int count = 0;
        for(int i=1; i< N+1; i++){
            if( table[i] == 1 ){
                count++;
            }

            if(count == n){
                cout << i << endl;
                break;
            }
        }
    }
	return 0;
}
