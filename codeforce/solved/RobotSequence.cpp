#include <bits/stdc++.h>
using namespace std;

int check(string s){
  int L = 0;
  int R = 0;
  int U = 0;
  int D = 0;
  for(int i=0; i< s.size(); i++){
    if( s[i] == 'L'){
      L++;
    }else if( s[i] == 'R'){
      R++;
    }else if( s[i] == 'U'){
      U++;
    }else if( s[i] == 'D'){
      D++;
    }
  }

  if( L == R && U == D){
    return 1;
  }else{
    return 0;
  }
}

int main()
{
  int num;
  scanf("%d\n", &num);

  string word;
  cin >> word;

  int count = 0;
  for (int len = 2; len <= num; len++)
  {
    for (int startIdx = 0; startIdx < num && startIdx+len-1 < num; startIdx++){
      string sub = word.substr(startIdx,len);
      count += check(sub); 
    }
  }

  cout << count << endl;

  return 0;
}
