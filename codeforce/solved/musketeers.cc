#include <bits/stdc++.h>
#define INT_MAX __INT_MAX__
using namespace std;

int AM[4100][4100];   // adj matrix
vector<int> AL[4100]; //adj list

class FoundTable
{
  public:
    bool checkIfExist(int a, int b, int c)
    {
        int arr[] = {a, b, c};
        sort(arr, arr + 3);
        if (this->record.find(make_tuple(arr[0], arr[1], arr[2])) != this->record.end())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void storeKey(int a, int b, int c)
    {
        int arr[] = {a, b, c};
        sort(arr, arr + 3);
        this->record[make_tuple(arr[0], arr[1], arr[2])] = true;
    }

  private:
    map<tuple<int, int, int>, bool> record;
};

int main()
{
    int V = 0; // number of nodes
    int E;     // number of edges
    scanf("%d %d\n", &V, &E);

    memset(AM, 0, sizeof AM);

    for (int i = 0; i < 4100; i++)
    {
        AL[i] = vector<int>();
    }

    // scanning edges into adj list
    for (int i = 0; i < E; i++)
    {
        int node1, node2;
        scanf("%d %d\n", &node1, &node2);
        AM[node1 - 1][node2 - 1] = 1;
        AM[node2 - 1][node1 - 1] = 1;

        AL[node1 - 1].push_back(node2 - 1);
        AL[node2 - 1].push_back(node1 - 1);
    }

    //checking through each node
    FoundTable foundTable = FoundTable();
    int minReco = 1e9;
    for (int A = 0; A < V; A++) //node starts from 1 to V
    {
        //find 2 friends
        for (int &B : AL[A])
        {
            for (int &C : AL[A])
            {
                if (B == C)
                {
                    continue;
                }

                //this tuple has been checked before
                if (foundTable.checkIfExist(A, B, C))
                    continue;

                //check if B and C are friends first
                if (AM[B][C] != 1)
                {
                    // B and C are not friend
                    continue;
                }

                // B and C are friend case: sum recognition
                int rec = AL[A].size() + AL[B].size() + AL[C].size() - 6;
                minReco = min(rec, minReco);

                //mark this as searched
                foundTable.storeKey(A, B, C);
            }
        }
    }

    if (minReco == 1e9)
    {
        minReco = -1;
    }
    cout << minReco << endl;
    return 0;
}
