#include<bits/stdc++.h>
#define MAX 1000000
using namespace std;
typedef pair<int,int> pii;

int main(){
    int n, m;
    cin >> n >> m;
    int searched[MAX];
    memset(searched, 0, sizeof searched);

    list<pii> que; // pair< node, step>

    que.push_back({n,0});

    while(que.size() != 0){
        pii u = que.front();
        que.pop_front();
        if( u.first == m ){
            //print out the solution
            cout << u.second << endl;
            break;
        }

        searched[u.first] = 1;

        //remember to check index out of bound
        if(u.first-1 >= 0 && searched[u.first-1] == 0){
            que.push_back({u.first-1, u.second+1});
        }

        //if the number is already greater than target, don't multiply it
        if(u.first < m && searched[u.first*2]== 0){
            que.push_back({u.first*2, u.second+1});
        }
    }

    return 0;
}
