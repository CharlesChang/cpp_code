#include <bits/stdc++.h>
using namespace std;

int main()
{
  multiset<int> beauty;
  int num;
  scanf("%d\n", &num);
  for (int i = 0; i < num; i++)
  {
    int n;
    scanf("%d", &n);
    beauty.insert(n);
  }

  vector<int> line;
  while (beauty.size() != 0)
  {
    multiset<int>::iterator itt = beauty.begin();
    while (itt != beauty.end())
    {

      line.push_back(*itt);
      auto newItt = beauty.upper_bound(*itt);
      beauty.erase(itt);
      itt = newItt;
    }
  }

  int count = 0;
  for (int i = 1; i < line.size(); i++)
  {
    if (line[i] > line[i - 1])
    {
      count++;
    }
  }

  cout << count << endl;
  // for (auto &i : line)
  //   cout << i;
  // cout << endl;
  return 0;
}