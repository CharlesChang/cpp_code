#include<bits/stdc++.h>
#define MAX 110

using namespace std;
typedef pair<int, int> pii;
typedef tuple<int,int> tii;
vector<pii> AL[MAX];//<node, color>

int doSomething(int source, int target){
    list<tii> que; // <node, color>
    int visited[MAX][MAX];//node, color
    memset(visited, 0, sizeof visited);
    
    unordered_set<int> reachableColor;
    //all out-going color from source is done
    fill(visited[source], visited[source]+MAX, 1);
    //pushing in starting nodes
    for(auto &neighbor: AL[source]){
        //cout << neighbor.first << "," << neighbor.second << endl;
        que.push_back(make_tuple(neighbor.first, neighbor.second));
    }

    //start looping neighbor
    while(que.size() != 0){
        tii data  = que.front();
        que.pop_front();
        int currU = get<0>(data);
        int currColor = get<1>(data);
        //cout << "currently At = " << currU << ", color = " << currColor << endl; 

        //skip condition
        if(visited[currU][currColor] == 1) continue;

        //found the path for this color
        if(currU == target){
            reachableColor.insert(currColor);
        }

        //action
        visited[currU][currColor] = 1;

        for(auto &currV: AL[currU]){
            //only enque edge of the same color
            if(currV.second != currColor) continue;
            que.push_back(make_tuple(currV.first, currV.second));
            
        }
    }
    
    return reachableColor.size();
}


int main(){
    int V, E;

    //initialize variables
    fill(AL, AL+MAX, vector<pii>());

    cin >> V >> E;
    for(int i=0; i< E; i++){
        int u, v, color;
        cin >> u >> v >> color;
        // convert 1 based to 0 based, undirected
        AL[u-1].push_back({v-1, color});
        AL[v-1].push_back({u-1, color});
    }

    int q;
    cin >> q;
    for(int i=0; i< q; i++){
        int u, v;
        cin >> u >> v;
        //convert 1 based to 0 based
        cout << doSomething(u-1, v-1) << endl;
    }

    return 0;
}


