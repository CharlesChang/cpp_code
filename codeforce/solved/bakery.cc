#include<bits/stdc++.h>
#define MAXCITY 100010
using namespace std;
typedef vector<pair<int,int>> vpii;
typedef vector<int> vi ;


int setOfStorage[MAXCITY];// check if node is a storage
vector<int> collection;
vpii AL[MAXCITY]; // pair< node, weight>

int main(){

    int city, road, storage;

    cin >> city >> road >> storage;

    //initialize some variables
    memset(setOfStorage, 0, sizeof setOfStorage);
    fill(AL, AL+MAXCITY, vpii());
//    for(int i=0; i< MAXCITY; i++){
//        AL[i] = vpii();
//    }

    //reading the inputs
    for(int i=0; i< road; i++){
        int v, u, weight;
        cin >> v >> u >> weight;
        
        //undirected, 1 based
        AL[v-1].push_back({u-1,weight});
        AL[u-1].push_back({v-1,weight});
    }

    //reading storage
    for(int i = 0; i< storage; i++){
        int n;
        cin >> n;
        //1 based
        setOfStorage[n-1] = 1;
        collection.push_back(n-1);
    }

    //start of the logic
    int minDist = 2e9;
    for(int &st: collection){
        // loop through each neightbor
        for(auto &node : AL[st]){
            if(setOfStorage[node.first] == 1) continue;
            minDist = min(minDist, node.second);
        } 
    }
    if(minDist == 2e9){
        cout << -1 << endl;
    }else{
        cout << minDist << endl;
    }
    return 0;
}
