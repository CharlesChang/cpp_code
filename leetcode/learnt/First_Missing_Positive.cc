//https://leetcode.com/problems/first-missing-positive/description/
#include<bits/stdc++.h>
using namespace std;
class Solution
{
public:
    int firstMissingPositive(vector<int> &A)
    {
        int n = A.size();
        for(int i = 0; i < n; ++ i)
            while(A[i] > 0 && A[i] <= n && A[A[i] - 1] != A[i])
                swap(A[i], A[A[i] - 1]);
        
        for(int i = 0; i < n; ++ i)
            if(A[i] != i + 1)
                return i + 1;
        
        return n + 1;
    }
};

int main(){
    Solution s;
    int ans;
    while(cin >> ans){
        cout << "expecting: " << ans << endl;
        vector<int> vt;
        int num;
        char c;
        while(scanf("%d%c", &num, &c)){
            vt.emplace_back(num);

            if(c != ' ') break;
        }
        cout << "Get: "<< s.firstMissingPositive(vt) << endl;
        for(int i=0; i < vt.size(); i++){
            cout << vt[i]<< " ";
        }
        cout << endl;

    }
    return 0;
}
