#include<bits/stdc++.h>
using namespace std;

typedef unsigned int ui;
class Solution {
public:
    int findPaths(int m, int n, int N, int i, int j) {
        int grid[m][n];
        memset(grid, 0, sizeof grid);

        list<tuple<int,int,int>> que;
        que.emplace_back(i,j,0);

        ui count = 0;
        int dc[4] = { 1 , 0 , -1, 0 };
        int dr[4] = { 0 , 1 ,  0, -1 };

        while(!que.empty()){
            int r = get<0>(*que.begin());
            int c = get<1>(*que.begin());
            int step = get<2>(*que.begin());
            que.erase(que.begin());

            if(step == N) continue; //kill this node

            for(int i=0; i< 4; i++){
                int nr = r+dr[i];
                int nc = c+dc[i];

                if( nr <0 || nr >= m || nc < 0 || nc >= n){
                    //out of bound => found
                    count++;
                }else{
                    que.emplace_back(nr, nc, step+1);
                }
            }
        }

        return  count ;
    }
};
int main(){
    Solution s;
    cout << s.findPaths(2,2,5,0,0) << endl;
    cout << s.findPaths(4,4,5,2,1) << endl;

    return 0;
}
