#include<bits/stdc++.h>
using namespace std;
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


typedef vector<int> vi;
class Solution {
    public:
        vector<int> largestValues(TreeNode* root) {
            vi ans;
            if(root == NULL){
                return ans;
            }

            list<pair<TreeNode*, int>> que;
            que.emplace_back(root, 0);
            
            //bfs
            while(!que.empty()){
                auto p = que.begin();
                TreeNode* node = p->first;
                int step = p->second;
                que.erase(que.begin());

                if( ans.size() == step){
                    //if this level no value yet
                    ans.emplace_back(node->val);
                }else{
                    //else we compare value
                    ans.back() = max( ans.back(), node->val );
                }


                if(node->left != NULL){
                    que.emplace_back(node->left, step+1);
                }

                if(node->right != NULL){
                    que.emplace_back(node->right, step+1);
                }
            }



            return ans;
        }
};
