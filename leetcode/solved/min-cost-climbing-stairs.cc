#include<bits/stdc++.h>
using namespace std;

class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        set<pair<int,int>> que; //pair< dist, node >
        que.insert({cost[0], 0});
        que.insert({cost[1], 1});
        
        
        int dist[cost.size()];
        fill(dist, dist+cost.size(), 1e9);

        while(!que.empty()){
            auto pr = *(que.begin());
            que.erase(que.begin());

            int ed = pr.first;
            int u = pr.second;
            if(u == cost.size()-1){
                return ed;
            }

            for(int i=1; i<=2;i++){
                if(ed + cost[u+i] > dist[u+i]){
                    dist[u+i] = ed + cost[u+i];
                    que.insert({dist[u+i], u+i});
                }    
            }
        }
        return 0;    
    }

};


int main(){
    Solution s;
    vector<int> i = {0,0,0,0};
    cout << s.minCostClimbingStairs(i) << endl;
    return 0;
}
