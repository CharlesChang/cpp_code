#include<bits/stdc++.h>
using namespace std;
class Solution {
    public:
        vector<string> topKFrequent(vector<string>& words, int k) {
            unordered_map<string, int> freq;

            //record frequency
            for(string &s:words){
                freq[s]++;
            }

            vector<pair<int, string>> vt;
            for(auto itt = freq.begin(); itt !=freq.end(); itt++){
                vt.emplace_back(itt->second, itt->first);
            }

            sort(vt.begin(), vt.end(), [&](pair<int,string> const &p1, pair<int, string> const &p2){
                if(p1.first > p2.first) return true;
                else if( p1.first == p2.first && p1.second < p2.second ){
                    return true;
                }else{
                    return false;
                }
            });

            vector<string> ans;
            int i = 0;
            for(auto itt = vt.begin(); itt != vt.end(); itt++){
                if( i == k ) break;
                i++;
                ans.emplace_back(itt->second);
            }

            return ans;

        }
};
int main(){
    Solution s;
    
    vector<string> input1 = {"i", "love", "leetcode", "i", "love", "coding"}; 
    vector<string> a = s.topKFrequent(input1, 2);
    for(auto &e: a) cout << e << " ";
    cout << endl;

    vector<string> input2 = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}; 
    vector<string> b = s.topKFrequent(input2, 4);
    for(auto &e: b) cout << e << " ";
    cout << endl;



    return 0;
}
