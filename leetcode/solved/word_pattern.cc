//https://leetcode.com/problems/word-pattern/description/

/*
 * Buggy: 2 char cannot map to the same string
 */
#include<bits/stdc++.h>
using namespace std;

class Solution {
public:
    bool wordPattern(string pattern, string str) {
        vector<string> res;
        stringstream ss(str);
        string item;

        //split string 
        while(getline(ss, item, ' ')){
            res.emplace_back(item);
        }
//        for(auto &e: res){
//            cout << e << " ";
//        }

        unordered_map<char, string> mapper;
        set<string> st;

        if( res.size() != pattern.size() ){
            return false;
        }

        for(int i=0; i< pattern.size(); i++){
            if( mapper.find(pattern[i]) == mapper.end()){
                //if this symbol has not been established

                if( st.find(res[i]) != st.end()){
                    return false;
                }
                mapper[pattern[i]] = res[i];
                st.insert(res[i]);
            }else{
                if(mapper[pattern[i]] != res[i]){
                    return false;
                }
            }

        }

        return true;
    }
};
int main(){
    Solution s;
    string s1 = "dog dog dog dog";
    string s2 = "aabb";

    cout << s.wordPattern(s2, s1) << endl;
    cout << s.wordPattern("abba", "dog cat cat dog") << endl;
    return 0;
}
