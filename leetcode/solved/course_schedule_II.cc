#include<bits/stdc++.h>
using namespace std;

using vpii = vector<pair<int,int>>;
using vi = vector<int>;
class Solution {
    public:
        vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {

            //corner cases
            if(prerequisites.size() == 0){
                vector<int> order;
                for(int i=0; i< numCourses; i++){
                    order.emplace_back(i);
                }
                return order;
            }


            //generate adj
            vector<int> adj[numCourses];
            for(auto &e: prerequisites){
                adj[e.second].emplace_back(e.first);
            }
            
            int visited[numCourses];
            fill(visited, visited+numCourses, 0);

            //try each node
            list<int> stak;
            bool cycle = false;
            for(int i=0; i< numCourses; i++){
                this->topo_sort(stak, visited, adj, i, cycle);
            }


            vector<int> order;
            if(cycle){
                return order;
            }
            for(auto itt = stak.rbegin(); itt !=stak.rend(); itt++){
                order.emplace_back(*itt);
            }

            return order;
        }

    private:
        void topo_sort(list<int> &stak, int visited[], vi adj[], int node , bool &cycle){
            if(cycle) return; //terminate if cycle found

            if(visited[node] == 2) return;

            //cycle detection
            if(visited[node] == 1){
                cycle = true;
                return;
            }

            visited[node] = 1;
            for(auto &e: adj[node]){
                this->topo_sort(stak,visited, adj, e, cycle);
            }
            visited[node] = 2;
            stak.emplace_back(node);
        }

};

int main(){
    int n;
    while(cin >> n){
        Solution s;
        int a,b;
        char c;
        vector<pair<int,int>> pre;
        while(scanf("%d %d%c",&a,&b,&c)  ){
            pre.emplace_back(a,b);
            if(c != ' ') break;
        }
       
        vector<int> ans = s.findOrder(n, pre);
        
        //print solution

        cout << "solution = ";
        for(auto &e: ans) cout << e << " ";
        cout << endl;

        //print expected answer
        cout << "expected = ";
        while(scanf("%d%c", &a, &c)){
            cout << a << " ";
            if(c != ' ') break;
        }
        cout << endl << endl;
         
    }

    return 0;
}
