#include<bits/stdc++.h>
using namespace std;

class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        int buckets[26];
        fill(buckets, buckets+26, 0);
        
        for(char &e: magazine){
            buckets[e-'a']++;
        }

        int buckets2[26];
        fill(buckets2, buckets2+26, 0);
        for(char &e: ransomNote){
            buckets2[e-'a']++;
        }

        for(int i=0; i< 26; i++){
            if( buckets2[i] > buckets[i] ) return false;
        }
        return true;
    }
};

int main(){
    Solution s;
    cout <<  "expect 0. get " << s.canConstruct("a", "b") << endl;
    cout <<  "expect 0. get " << s.canConstruct("aa", "ab") << endl;
    cout <<  "expect 1. get " << s.canConstruct("aa", "aab") << endl;
    cout <<  "expect 0. get " << s.canConstruct("ihgg", "ch") << endl;
    return 0;
}
