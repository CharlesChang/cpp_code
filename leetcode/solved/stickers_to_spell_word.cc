#include<bits/stdc++.h>
using namespace std;
const int B_SIZE = 26;
class Solution {

public:
    int minStickers(vector<string>& stickers, string target) {
        int n = stickers.size();

        //construct profiles for each stickers
        vector<vector<int>> profiles(n, vector<int>(26, 0));
        for(int i=0; i< n; i++){
            for(char &c : stickers[i]){
                profiles[i][c-'a']++;
            }
        }
        unordered_map<string, int> table;
        table[""] = 0;

        return this->search(table, profiles, target);
    }


private:
    int search( unordered_map<string, int> &table, vector<vector<int>> &profiles, string target ){
        //visited
        if(table.find(target) != table.end()) return table[target];

        //construct profile for the target
        int tar[26]; memset(tar, 0 , sizeof tar);
        for(char &e: target)  tar[e-'a']++;
        int min_from_substring = INT_MAX;
        //try every sticker
        for(int i=0; i< profiles.size(); i++){
            //if(profiles[i][target[0]-'a'] == 0) continue;
            string newS;
            //apply the stick onto the target
            for(int j=0; j< 26; j++){
                if( tar[j] > profiles[i][j]){
                    //constructing leftover string
                    newS += string( tar[j]-profiles[i][j], j+'a' );
                }
            }
            if(newS == target) continue;
            int tmp = this->search(table, profiles, newS);
            if(tmp != -1){
                min_from_substring = min(min_from_substring, tmp+1);
            }
        }
        table[target] = min_from_substring == INT_MAX? -1: min_from_substring;
        return table[target];
    }
};
int main(){
    Solution s;

    vector<string> stickers = { "with", "example", "science" };
    string tar =  "thehat";
    cout << s.minStickers(stickers, tar) << endl;
    return 0;
}
