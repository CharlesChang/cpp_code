//https://leetcode.com/problems/asteroid-collision/description/
#include<bits/stdc++.h>
using namespace std;
class Solution {
public:
    vector<int> asteroidCollision(vector<int>& asteroids) {
        list<int> ast;
        for(int &i:asteroids) ast.emplace_back(i);

        bool changed = true;
        for(auto itt= ast.begin(); itt != ast.end(); itt++){
            if(itt == prev(ast.begin(),1)) continue;
            //current positive and current not the last
            if( *itt > 0 && next(itt,1) != ast.end()){
                //next is negative
                if( *(next(itt,1)) < 0 ){
                    //check size
                    int a = *itt;
                    int b = abs(*next(itt,1));

                    if( a > b ){
                        itt--;
                        ast.erase(next(itt,2));
                    }else if( a < b){
                        itt--;
                        ast.erase(next(itt,1));
                    }else{
                        itt--;
                        ast.erase(next(itt,1)); //erase next
                        ast.erase(next(itt,1)); //erase next next 
                    }

                    // move back 1 more time, hence next loop will check current
                    if(itt != --ast.begin()) itt--; 
                } 
            }
        }

        vector<int> ans;
        for(int &i:ast) ans.emplace_back(i);
        return ans;

    }

    void printList(list<int> l){
        for(auto &e:l) cout << e << " ";
        cout << endl;
    }
    void printList(vector<int> l){
        for(auto &e:l) cout << e << " ";
        cout << endl;
    }
private:
};

int main(){
    Solution s;
    int cases;
    cin >> cases;
    while(cases--){
        int a;
        char c;
        vector<int> input;
        while(scanf("%d%c", &a, &c)){
            input.emplace_back(a);
            if(c != ' '){
                vector<int> output = s.asteroidCollision(input);
                cout << "answer = ";
                for(int &i:output) cout << i << " ";
                cout << endl;
                break;
            } 
        }

        scanf("\n"); // next line;

        cout << "Expects = ";
        while(scanf("%d%c", &a, &c)){
            cout << a << " "; 
            if(c != ' ') break;
        }
        cout << endl;
        scanf("\n"); // next line;
    }

    return 0;
}
