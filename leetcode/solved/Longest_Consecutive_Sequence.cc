/*
 * This solution is based on other people's solution
 */
#include<bits/stdc++.h>
using namespace std;
class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_set<int> mapper(nums.begin(), nums.end());
        int ans = 0;
        for(int const &i:mapper){
            if(mapper.count(i-1) == 0){ //this is the beginning
                int count = 1;
                while(mapper.count(i+count) == 1){
                    count++;
                }
                ans = max(ans, count);

            }
        }

        return ans;
    }
    
};
int main(){
    Solution s;

    return 0;
}
