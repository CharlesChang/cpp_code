#include<bits/stdc++.h>
using namespace std;

class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        unordered_map<int,int> mapper; //num, frequency
        
        int highestF = 0;
        //recording frequency
        for(auto &i: nums){
            mapper[i]++;
            
            //keep track of the most frequent
            if(mapper[i] > highestF){
                highestF = mapper[i];
            }
        }
        
        vector<int> freq_table[highestF+1];

        for(auto itt = mapper.begin(); itt != mapper.end(); itt++){
            freq_table[ itt->second ].emplace_back( itt->first );            
        }

        vector<int> ans;
        int count = 0;
        for(int i= highestF; i >= 0; i--){
            if(freq_table[i].size() != 0 ){
                for(int &i :freq_table[i]){
                    ans.emplace_back(i);
                    count++;
                }

                if(count >= k){
                    break;
                }
            }
        }
        
        return ans;
    }
};

int main(){
    vector<int> g = { 1,1,1,2,2,3 };
    Solution s;

    vector<int> k = s.topKFrequent(g, 2); 
    for(auto &i:k){
        cout << i << " "<< endl;
    }

    return 0;
}
