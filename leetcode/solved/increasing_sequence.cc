//https://leetcode.com/problems/increasing-subsequences/description/
/*
 * Note:
 * This is a special case of finding subsets
 * The solution is affected by the given sequence. Hence you are not required to sort.
 *
 *
 * This solution is modelled after given solution. I do not claim credibility in the algorithm used here
 */
#include<bits/stdc++.h>
using namespace std;

class Solution {
    public:
        vector<vector<int>> findSubsequences(vector<int>& nums) {
            vector<int> seq;
            vector<vector<int>> ans;
            //start recursion
            this->helper(nums, ans, seq , 0);
            return ans;

        }
        void helper(vector<int> &nums, vector<vector<int>> &ans, vector<int> &seq ,int pos){
            if(seq.size() > 1){
                ans.emplace_back(seq);
            }

            unordered_map<int, bool> exist;
            for(int i=pos; i< nums.size(); i++){
                if(exist.find(nums[i]) == exist.end() && (seq.empty() || nums[i] >= seq.back()) ){
                    seq.emplace_back(nums[i]); 
                    this->helper(nums, ans, seq, i+1);
                    seq.pop_back(); //backtrack

                    exist[nums[i]] = true;
                }

            }
        }

        void printVector(vector<vector<int>> &ans){

            for(vector<int> &e:ans){
                cout << "[ ";
                for(int &r:e){
                    cout << r << ",";
                }
                cout << " ],";
            }
            cout << endl << "size = " << ans.size() << endl;
        }
    private:
};

int main(){
    Solution s;
    vector<int> input = {4,6,7,7};
    vector<vector<int>> ans = s.findSubsequences(input);
    s.printVector(ans);

    return 0;
}
