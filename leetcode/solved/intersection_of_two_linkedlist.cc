#include<bits/stdc++.h>
using namespace std;

class ListNode{
    public:
        int val;
        ListNode *next;
        ListNode(int x):val(x), next(NULL){}
};
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        
        unordered_map<ListNode*,int> mapper; // address , bool
        while(headA != NULL){
           mapper[headA] = 1;
           headA = headA->next;
        }
        
        while(headB != NULL){
            if(mapper.find(headB) != mapper.end()){
                return headB;
            }

            headB = headB->next;
        }
        
        return NULL;
    }
};
