#include<bits/stdc++.h>
/*
 * This solution works but too slow
 * The Greedy solution is to sort by increasing ending time and 
 * greedily add from first element as long as the element fits,
 * But why that works I don't know
 */
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef unordered_map<string, int> umsi;
class Solution {
public:
    int findLongestChain(vector<vector<int>>& pairs) {
       
        //sort to incrasing first value
        int max_len = 0;
        sort(pairs.begin(), pairs.end(), [&](vi const &p1, vi const &p2){
            if( p1.front() < p2.front() ){
                return true;
            }
            return false;
        }); 

        vi curr;//to record current sequence of selected index
        unordered_map<string, int> exist;
        this->helper(pairs, 0, curr, max_len, exist);
        return max_len;
    }
private:
    void helper(vvi &pairs, int pos, vi &curr, int &max_len, umsi &exist){
        //check if visited
        string s = this->vToS(curr);
        if( exist.find(s) != exist.end() ) return;
        exist[s] = 1;

        //tap into possible solution
        if( curr.size() > max_len ) max_len = curr.size();

        for(int i=pos; i< pairs.size(); i++){
            if(this->isSafe(pairs,curr, pairs[i])){
                curr.emplace_back(i);
                this->helper(pairs, pos+1, curr, max_len, exist);
                curr.pop_back();
            }
        }
    }

    bool isSafe(vvi pairs, vi &curr, vi &p){
        if(curr.size() == 0){
            return true;
        }
        if( pairs[curr.back()].back() < p.front()){
            return true;
        }
        return false;
    }

    string vToS(vi vt){
        string s;
        for(int i=0; i<vt.size(); i++){
            s+= to_string(vt[i]);
        }
        return s;
    }
};
int main(){
    Solution s;

    int n;
    while(cin >> n){
        int a,b;
        vector<vi> pairs;
        for(int i=0; i< n; i++){
            cin >> a >> b;
            vi tmp;
            tmp.emplace_back(a);
            tmp.emplace_back(b);
            pairs.emplace_back(tmp);
        }

        cin >> a;
        cout << "Expected: " << a << endl;
        cout << "Gets: " << s.findLongestChain(pairs) << endl;
    }

    return 0;
}
