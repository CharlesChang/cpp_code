/*
 * Status:
 * Buggy 
 * I have come to believe trial and error solution won't work. 
 * This question has to be calculated mathematically
 */
#include<bits/stdc++.h>
using namespace std;

class Solution {
public:
    int leastInterval(vector<char> &tasks, int n){
        pair<int,char> freq[26]; // frequency, task

        int cd[26];
        memset(cd, 0, sizeof cd);

        int task_left = tasks.size();
        for(auto &c: tasks){
            freq[c-'A']  = {freq[c-'A'].first +1, c };
        }
        sort(freq, freq+26);

//        for(int i=0; i< 26; i++){
//            printf("{ %d, %c } ",freq[i].first, freq[i].second );
//        }
//        cout << endl; 

        vector<char> cpu_time;
        while(task_left){
            bool assigned = false;
            for(int i= 25; i >= 0; i--){ //looping from highest frequency task

                if(cd[i] <= 0 && !assigned && freq[i].first != 0){
                    //recover task char
                    char a = freq[i].second;

                    //if not on cooldown
                    cpu_time.emplace_back(a);

                    freq[i].first--;
                    task_left--;
                    cd[i] = n;
                    assigned = true;
                }else{
                    if(cd[i] > 0){
                        cd[i]--;
                    }
                }

                if( i == 0 && !assigned ){
                    cpu_time.emplace_back('\0');
                }
            } 
        }

        this->out(cpu_time);
        return cpu_time.size();
    }

private:
    void out(vector<char> &vc) const {
        for(auto &i: vc){
            cout << i << " ";
        }
        cout << endl;
    }
};
int main(){

    Solution s;

    vector<char> tasks;
    int n, ans;
    string text;
    while(cin >> text, cin >> n, cin >> ans){
        tasks.clear();
        for(int i=0; i< text.size(); i++){
            char a = text[i];
            if(a != '[' && a!= ']' && a != '"' && a != ','){
                tasks.emplace_back(a);
            }
        }
        cout << s.leastInterval(tasks, n) << endl;
        cout << "Answer = " << ans << endl;
    }
    return 0;

}
