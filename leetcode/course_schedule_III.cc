#include<bits/stdc++.h>
using namespace std;
class Solution {
public:
    int scheduleCourse(vector<vector<int>>& courses){
        //sort by increasing ending time
        sort(courses.begin(), courses.end(), 
             [](auto &vt1, auto vt2){ return vt1[1] <  vt2[1]; });

        //store each course , with increasing duration
        multiset<int> pq;

        int sum = 0;
        
        for(vector<int> &e: courses){
            //if next one fits in, we fit
            if(sum+ e[0] <= e[1] ){
                sum += e[0];
                pq.emplace(e[0]);
            }else if( (*pq.rbegin()) > e[0] ){
                //try to swap out a longest one from the pq
                sum  -= (*pq.rbegin()) + e[0];
                pq.erase(--pq.end());
                pq.emplace(e[0]);
            }
        }

        return pq.size();
        

    }
};

int main(){

    return 0;
}
