#include<bits/stdc++.h>
/*
 * Note: 
 * compltely wrong approach since this question requires sub-string to be continuous. Cannot use the normal way to calculate;
 *
 */
using namespace std;
class Solution {
    public:
        string longestPalindrome(string s) {
            int n = s.size();
            int table[n][n];
            pair<int,int> sol[n][n];

            for(int r=1;  r<= n; r++){ 
                for(int c = 0; c < n-r+1; c++){
                    int end = c+r-1;
                    if(r == 1){
                        table[c][end] = 1;
                        continue;
                        sol[c][end] = {c,end};
                    }    
                    if(s[c] == s[end]){
                        if(r == 2){
                            table[c][end] = 2;
                            sol[c][end] = {c, end};
                        }else{
                            table[c][end] = 2+table[c+1][end-1];
                            auto pr = sol[c+1][end-1]; 
                            if(pr.first == c -1 && pr.second == end+1){
                                sol[c][end] = {pr.first-1, pr.second+2}; 
                            }else{
                                sol[c][end] = {pr.first, pr.second};
                            }
                        }
                    }else{
                        if( table[c+1][end] > table[c][end-1] ){
                            table[c][end] = table[c+1][end];
                            sol[c][end] = sol[c+1][end];
                        }else{
                            table[c][end] = table[c][end-1];
                            sol[c][end] = sol[c][end-1];
                        }
                    }
                }
            }

            auto pr = sol[0][n-1];
            return s.substr(pr.first, pr.second- pr.first +1);
        }
};
