/*
 * Status
 * Incomplete, next time bro
 */
#include<bits/stdc++.h>
using namespace std;
class Solution {
    public:
        int shoppingOffers(vector<int>& price, vector<vector<int>>& special, vector<int>& needs) {
            //buying without offer
            int min_cost = min(  this->buyWithoutSpecial(price, needs), this->search(price, special, needs, min_cost)  );

            return min_cost;
        }

    private:
        int search(vector<int> &price, vector<vector<int>> &special, vector<int> needs, int &min_cost){
            /*
             * backtracking algothm, there are special.size() ways +1 (buy without any special);
             * Every stage of recursion will consider each case and if they satisfy the constraint
             */

            for(int i=0; i< special.size(); i++){

                //checking if this special fits the contraint         
                //by seeing if taking this special will cause any item to overshoot
                bool canUseSpecial = true;
                for(int j=0; j < special[i].size(); j++){
                    if(needs[j] - special[i][j] < 0 ){
                        canUseSpecial = false;
                        break;
                    }
                }
                if(canUseSpecial){

                }else{
                    //use direct purchase to fill up the rest
                }

            }
        }

        int buyWithoutSpecial(vector<int> &price, vector<int> &needs){
            int total = 0;
            for(int i=0; i< needs.size(); i++){
                total += needs[i] * price[i];
            }

            return total;
        }
};
int main(){

    return 0;
}
