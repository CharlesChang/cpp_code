public class Search {
    public static void main(String args[]) {
        int input[] = {5, 7, 7, 8, 8, 10};
        int answer[] = Search.searchRange(input, 8);

        System.out.println(answer[0]);
        System.out.println(answer[1]);

    }

    public static int[] searchRange(int[] nums, int target) {
        int answer[] = new int[2];
        int len = nums.length;
        int x = 0;
        int lb = 0, hb = len;
        boolean lowerBound_found = false;

        x = len/2;
        while (lowerBound_found == false) {
            if (nums[x] > target) {
                hb = x;
                x = (x + lb) / 2;
            } else if (nums[x] < target) {
                lb = x;
                x = (x + hb) / 2;
            } else {
                if (x - 1 != -1 && nums[x - 1] == target) {
                    hb = x;
                    x = (x + lb) / 2;
                } else {
                    lowerBound_found = true;
                    answer[0] = x;
                }
            }
        }

        boolean upperBound_found = false;
        x = len/2;
        while (upperBound_found == false) {
            if (nums[x] > target) {
                hb = x;
                x = (x + lb) / 2;
            } else if (nums[x] < target) {
                lb = x;
                x = (x + hb) / 2;
            } else {
                if (x + 1 < len && nums[x + 1] == target) {
                    lb = x;
                    x = (x + hb) / 2;
                } else {
                    upperBound_found = true;
                    answer[1] = x;
                }
            }
        }


        return answer;
    }

}
