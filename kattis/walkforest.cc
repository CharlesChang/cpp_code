#include<bits/stdc++.h>
#define INF 1e9
using namespace std;

int main(){
    int V, E;

    while( cin >> V >> E ){
        if(V == 0){
            break;
        }

        vector<pair<int,int>> AL[V]; // pair<node, dist>
        for(int i=0; i< E; i++){
            int u,v,w;
            cin >> u >> v >> w;

            //from 1-based to 0-based
            AL[u-1].emplace_back(v-1,w);
            AL[v-1].emplace_back(u-1,w);
        }

        set<pair<int,int>> que; //pair< dist, node>
        que.insert({0,0});

        int dist[V];
        fill(dist, dist+V, INF);
        dist[0] = 0; //source

        int ways[V];
        fill(ways, ways+V, 0);
        ways[0] = 1;

        while(!que.empty()){
            //doing dijstras to cover every node
            auto u = *(que.begin()); // first = dist, second = node;
            que.erase(que.begin());


            //skip condition
            if(u.first > dist[u.second]) continue;
            if(u.second == 1){
                continue;
            }

            for(auto &v : AL[u.second]){  //v.first = node, v.second = weight

                if(dist[v.first] > dist[u.second] + v.second){
                    dist[v.first] = dist[u.second + v.second];

                    ways[v.first] = ways[u.second];
                    que.insert({dist[v.first], v.first}); //propagate
                }else if(dist[v.first] == dist[u.second] + v.second){
                    ways[v.first] += ways[u.second];
                }else if(v.first == 1){
                    ways[v.first] += ways[u.second];
                }
            }  
        }

        for(int i=0; i< V; i++){
            cout << "ways[" << i+1 << "] = " << ways[i] <<endl; 
        }
        cout << ways[1] << endl;
    }
    return 0;
}
