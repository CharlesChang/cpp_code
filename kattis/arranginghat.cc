//https://open.kattis.com/problems/arranginghat
#include<bits/stdc++.h>
using namespace std;

int isSorted(vector<string> &arr){
    
    for(int i=0; i < arr.size(); i++){
        if( stoi(arr[i]) > stoi(arr[i+1]) ){
            return i;
        }
    }
    return -1;
}

int main(){
    
    int n, m;
    cin >> n >> m;

    vector<string> arr;
    for(int i=0; i< n; i++){
        string str;
        cin >> str;
        arr.emplace_back(str);
    }
    
    list<vector<string>> que;
    que.emplace_back(arr);
    //start bfs
    while(que.size() != 0){
        vector<string> curr = que.front();
        que.pop_front();

    
        if( isSorted(curr) == -1){  //found, break and print answer
            for(int j=0; j< curr.size(); j++){
                cout << curr[j]<< endl;
            }
            break;
        }

        for(int i=0; i< n; i++){// for each element, try change 1 digit
            if(i == 0){ // first number
                //insert all possible cases
                for(int j=0; j< m; j++){
                            
                }                
            }else if(i == n-1){ // last number
                //insert all possible cases

            }else { //numbers in between 
                //insert all possible cases

            }
        }

    }

    return 0;
}
