#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int>  pii;
typedef vector<tuple<string,int,int>> vtsii;

class Rate{
    public:
        int from;
        int to;

        Rate(int from, int to){
            this->from = from;
            this->to = to;
        }

        
        bool isGreater(Rate r){
            if(r.to ==0 || r.from == 0){
                return true;
            }
            if(this->to/this->from > r.to/r.from){
                return true;
            }else{
                return false;
            }
        }


        bool  arbitrage(){
            if( this->to > this->from ){
                return true;
            }else{
                return false;
            }
        }
};

int main(){

    int C;
    while(true){
        scanf("%d", &C);
        if(C == 0){ //termination
            break;
        }
        vector<string> currency;
        for(int i=0; i< C; i++){
            string tmp;
            cin >> tmp;
            currency.emplace_back(tmp);
        }

        int R;
        cin >> R;

        unordered_map<string, vector<pair<string, Rate>>> AL; // currency, Rate
        for(int i=0; i < R; i++){
            string s1,s2;
            int r1 , r2; 
            cin >> s1 >>s2;
            scanf("%d:%d\n", &r1, &r2);
            AL[string(s1)].push_back(make_tuple(string(s2), Rate(r1,r2)));
//            cout << string(s1) << " " << string(s2) << " "<< r1 << ":"<<r2<<endl;
        }

        //do cycle finding
        bool arbitrage = false;
        for(auto &source: currency){ //run cycle finding on each of the currency
            if(arbitrage) break;

            //use bfs
            list<pair<string,Rate>> que; // node, Rate
            que.push_back({source, Rate(1,1)});
            unordered_map<string, Rate> dist; //Currency,  Rate
            

            while(que.size() != 0){
                auto node = que.front();
                que.pop_front();
                string u = node.first;
                Rate r = node.second;

                //lazy deletion
                //early termination condition
                if(u == source){
                    if( r.arbitrage()  ){
                        arbitrage = true;
                        break;
                    }else{
                        continue;
                    }
                } 

                //step through neighbor
                for(auto &neighbor: AL[u]){
                    string v = neighbor.first;
                    Rate w = neighbor.second;
                    //
                    //handle the case where dist is unitialized
                    if( dist.find(v) == dist.end() || Rate(r.to*w.to, r.from*w.from).isGreater(dist[v]) ){
                     //if faction at next point < faction at current point*increment
                        //dist[v] = {deno*from, nume*to};
                        //que.push_back(make_tuple(v, deno*from, nume*to));
                    }
                }
            }
        }

        if(arbitrage){
            cout << "Arbitrage" << endl;
        }else{
            cout << "Ok" << endl;
        }
    }

    return 0;
}


/*
 * Note:
 *  1.IO string has been a problem
 */
