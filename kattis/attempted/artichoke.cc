#include<bits/stdc++.h>
using namespace std;
class Solution{
    public:
        double solve(int p, int a, int b, int c, int d, int n){
            function<double (int)> price = [=](int k){
                return p* ( sin(a*k+b)  + cos(c*k+d)+2 );
            };
            if(n < 2){
                return 0;
            }


            double val1 = price(1);
            double val2 = price(2);
            double peak, trough, max_diff = 0;
            bool rising;
            if(val1 > val2){
                peak = val1;
                trough = val2;
                max_diff = peak-trough;
                rising = false;
            }else{
                peak = val2;
                max_diff = 0;
                rising = true;
            }
            val1 = val2;
            for(int i=3; i<=n; i++){
                val2 = price(i);
                if(rising){
                    if(val2 > val1){
                        peak = val2;
                    }else{
                        trough = val2;
                        rising = false;
                        max_diff = max(max_diff, peak-trough);
                    }
                }else{
                    if(val2 < val1){
                        trough = val2;

                        max_diff = max(max_diff, peak-trough);
                    }else{
                        max_diff = max(max_diff, peak-trough);
                        rising =  true;

                        peak = val2;
                    }
                }

                //update previous value            
                val1 = val2;
            }

            return max_diff;
        }
};

int main(){

    int p, a,b,c,d,n;
    Solution s;
    while(cin >> p >> a >> b >> c >> d >> n){
        printf("%0.6f\n", s.solve(p, a,b,c,d,n));
    }
    return 0;
}
