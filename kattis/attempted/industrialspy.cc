#include<bits/stdc++.h>
using namespace std;

vector<string> genPermutation(string t){
    vector<string> ans;
    for(int i=0; i< t.size(); i++){
        swap(t[0],t[i]); // bring this to the first
        ans.push_back( genPermutation(t.substr(1)) );
        swap(t[0], t[i]); // restore original string
    }

}


int main(){
    string text;
    cin >> text;

}
