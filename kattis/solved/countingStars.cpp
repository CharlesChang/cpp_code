#include <bits/stdc++.h>
using namespace std;

void searchAndReplace(int i, int j, vector<vector<string>>&table)
{
  string current = table[i][j];
  if (current == "#" || current == "0")
  {
    return;
  }
  else
  {
    table[i][j] = "0";
    searchAndReplace(i - 1, j, table);
    searchAndReplace(i + 1, j, table);
    searchAndReplace(i, j - 1, table);
    searchAndReplace(i, j + 1, table);
  }
}

int main()
{
  int row, col;
  int testcases = 0;
  while (cin >> row >> col)
  {
    testcases++;
    vector<vector<string>> table(row+2, vector<string>(col+2, "0"));


    //loop through each
    for (int i = 1; i < row+1; i++)
    {
      string val;
      cin >> val; 

      for (int j = 0; j < val.length(); j++)
      {
        table[i][j+1] = val[j];
      }
    }


    int count = 0;
    for (int i = 1; i < row + 1; i++)
    {
      for (int j = 1; j < col + 1; j++)
      {
        // cout<< table[i][j];
        if (table[i][j] == "-")
        {
          count++;
          searchAndReplace(i, j, table);
        }
      }
    }

    cout<< "Case " << testcases << ": " <<  count << endl;
  }

  return 0;
}
