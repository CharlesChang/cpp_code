#include<bits/stdc++.h>
using namespace std;
typedef unsigned long int uli;

int main(){

    int n, m;
    unordered_map<uli,uli>  st;
    while(cin >> n >> m, (n || m)){
        st.clear();
        while(n--){
            uli k;
            cin >> k;
            st[k] = 1;
        }

        uli count = 0;
        while(m--){
            uli k;
            cin >> k;

            if( st[k] == 1 ){
                count++;
            }
        }
        cout << count << endl;
    }

    return 0;
}
