#include<bits/stdc++.h>
#define MAX 5010
using namespace std;
typedef unordered_map<string, int> umsi;
typedef unordered_map<string, vector<string>> umsvs;

void dfs(string source, umsvs &AL, bool &found, umsi &visited){
    //skip condition
    if(found == true) return;

    //mark
    visited[source] = 1;

    for(auto &v: AL[source] ){
        if( visited[v] == 1){
            found = true;
            return;
        }else if(visited[v]== 0){
            dfs(v, AL, found, visited);
        }
    }
    visited[source] = 2;
}



int main(){
    int flights;
    cin >> flights;

    unordered_map<string, vector<string>> AL; //city name, neighbor city
    for(int i=0; i< flights; i++){
        string c1, c2;
        cin >> c1 >> c2;
        AL[c1].emplace_back(c2);
    }

    string queryCity;
    while( cin >> queryCity ){
        //run the algorithm
        unordered_map<string, int> visited; 
        bool found = false;
        dfs(queryCity, AL, found, visited);

        if( !found ){
            cout << queryCity << " trapped"<< endl;
        }else{
            cout << queryCity << " safe" << endl;
        }
    }


    return 0;
}
