#include <bits/stdc++.h>
#define MAX 510
using namespace std;

int main()
{
    int n, m;
    cin >> n >> m;
    char table[n][m];

    //direction array
    int dx[] = {1, 0, -1, 0};
    int dy[] = {0, 1, 0, -1};

    //scanning each line as string
    for (int i = 0; i < n; i++)
    {
        scanf("%s", &table[i]);
    }

    int dist[MAX][MAX];
    memset(dist, -1, sizeof dist);
    dist[0][0] = 0;

    queue<pair<int, int>> que; //< x,y>

    que.push({0, 0});

    while (!que.empty())
    {
        auto p = que.front();
        que.pop();
        int x = p.first;
        int y = p.second;

        int multiplier = table[x][y] - '0';
        for (int i = 0; i < 4; i++)
        {
            //construct next position from current position
            int nextX = x + multiplier * dx[i];
            int nextY = y + multiplier * dy[i];

            //note that nextX == n is actually outside the grid
            if (nextX < 0 || nextX >= n || nextY < 0 || nextY >= m)
                continue; //out of grid
            if (dist[nextX][nextY] != -1)
                continue; //bfs already visit this before

            //update dist to next node, enqueue next node
            dist[nextX][nextY] = dist[x][y] + 1;
            que.push({nextX, nextY});
        }
    }

    printf("%d\n", dist[n - 1][m - 1]);

    return 0;
}
