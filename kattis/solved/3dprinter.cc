#include<bits/stdc++.h>
using namespace std;
class Solution{
    public:
        /* 
         * BFS search method
         */
        int search(int n){
            list<tuple<int,int,int>> que;  //num printer, num statues, days
            que.push_back(make_tuple(1,0,0));

            int visited[n+1][n+1];
            memset(visited, 0, sizeof visited);
            visited[1][0] = 1; //mark the origin

            while(!que.empty()){
                auto tp = que.front();
                que.pop_front();

                int no_printer = get<0>(tp);
                int no_statues = get<1>(tp);
                int no_day = get<2>(tp);
                if(no_printer + no_statues >= n ){
                    return no_day+1;
                }else{
                    for(int i=0; i <= no_printer; i++){
                        int np = no_printer +i;
                        int ns = no_statues + no_printer -i;
                        if(ns < 0 || np > n || ns > n) continue;
                        if( visited[np][ns] == 0 ){
                            que.push_back(make_tuple(np,ns, no_day+1));
                            visited[np][ns] = 1;
                        }
                    }
                }
            }
            return 0;
        }

        /*
         * Greedy Method
         */
        int greedy(int n){
            if(n == 1){ // special case
                return 1;
            }

            vector<pair<int, int>> attempt; //printer no.  , days
            int p = 1;
            int day = 0;
            while(p < n){
                p = p << 1;
                day++;
                attempt.emplace_back(p, day);
            }

            int minDay = n;
            for(auto &pr:attempt){
                int s = 0;
                int p = pr.first;
                int d = pr.second;
                while(s < n){
                    s+=p;
                    d++;
                }

                minDay = min(minDay, d);
            }
            
            return minDay;

        }

        int naive(int n){
            if(n == 1) return 1;

            int p=1, day = 0;
            while(p < n){
                p = p <<1;
                day++;
            }
            return day+1;
        }

        int yijin(int n){
            return ceil(log2(n))+1;
        }
};
int main(){

    int n;
    Solution s;
    //cout << s.greedy(n)<< endl;
    while(cin >> n){
        //cout << s.greedy(n) << endl; 
        //cout << s.greedy(n) << " : " << s.search(n) << endl; 
        cout << s.yijin(n) << endl;
    }

    return 0;
}
