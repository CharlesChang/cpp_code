#include<bits/stdc++.h>
using namespace std;

int main(){

    int C;
    cin >> C;

    while(C--){

        int N;
        cin >> N;
        int total = 0;
        int score[N];
        for(int i=0; i< N; i++){
            int tmp;
            cin >>tmp;
            total +=tmp;
            score[i] = tmp;
        }
        int average = total/N;
        int count = 0;
        for(int i=0; i< N; i++){
            if(score[i] > average){
                count++;
            }
        }

        printf("%.3f%%\n", (double)100*count/N);
        
    }
    return 0;
}
