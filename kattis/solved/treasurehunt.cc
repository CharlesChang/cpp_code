#include<bits/stdc++.h>
using namespace std;
/*
 * Note:
 * 1. ways to scan char as string into table
 */
int main(){
    int R,C;
    cin >> R >> C;
    char table[R][C];
    int explored[R][C];
    memset(explored, 0, sizeof explored);

    for(int i= 0; i < R; i++){
        scanf("%s", &table[i][0]);
    }
    
    pair<int,int> curr_loc = {0,0}; //upper left corner
    int step = 0;
    while(true){
        char i = table[curr_loc.first][curr_loc.second];

        if( explored[curr_loc.first][curr_loc.second] == 1 ){
            cout << "Lost" << endl;
            break;
        }
        explored[curr_loc.first][curr_loc.second] = 1;  //mark explored 

        if(i == 'T'){
            cout << step << endl;
            break;
        }
        int nr = curr_loc.first;
        int nc = curr_loc.second;
        if( i == 'N' ) nr--;
        else if(i == 'S') nr++;
        else if(i == 'W') nc--;
        else if(i == 'E') nc++;
        if( nr >= R || nr < 0 || nc >= C || nc < 0 ){
            cout << "out" << endl;
            return 0;
        }
        curr_loc = { nr, nc };
        step++;
    }
    
    return 0;
}


/*
 *Errors:
 * 1. the natural table goes from top left to bottom right
 *      a. going north is actually row--
 *      b. going south is row++
 * 2. Don't forget to handle infinite loop case
 */
