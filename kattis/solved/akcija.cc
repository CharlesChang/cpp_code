#include<bits/stdc++.h>

using namespace std;

int main(){
    
    int n;
    cin >> n;
    multiset<int> l;
    while(n--){
        int m;
        cin >> m;
        l.insert(m);
    }

    int total = 0;
    auto itt = l.rbegin();
    int count = 1;
    while(itt != l.rend()){
        //cout << "itt= " << *itt << endl;
        if(count %3 != 0){
            total += *itt;
        }
        itt++;
        count++;
    }
    cout << total << endl;
    return 0;
}
