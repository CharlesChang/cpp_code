#include<bits/stdc++.h>
using namespace std;

int main(){
    int V, E;
    cin >> V >> E;

    vector<pair<int,int>> AL[V]; // node, weight
    for(int i=0; i< E; i++){
        int u, v, w;
        cin >> u >> v >> w;

        AL[u].emplace_back(v, w);
    }

    int source, target;
    cin >> source >> target;

    //start of dijstra's

    set<pair<int,int>> que; // pair< dist, node>
    que.insert({0, source});

    int dist[V];
    fill(dist, dist+V, 1e9);
    dist[source] = 0;

    int ways[V];
    fill(ways, ways+V, 0);
    ways[source] = 1;


    while( !que.empty() ){
        auto p = *(que.begin()); // dist, node
        que.erase(que.begin());

        //skip condition
        if(p.first > dist[p.second]){
            continue;
        }

        if(p.second == target){
            break;
        }

        for(auto &v: AL[p.second]){ // node , weight

            if( dist[v.first] > p.first  + v.second){
                dist[v.first] = p.first + v.second;
                ways[v.first] = ways[p.second];
                que.insert({dist[v.first], v.first});
            }else if( dist[v.first] ==  p.first+v.second ){
                ways[v.first] += ways[p.second];
            }
        }
    }

    cout << ways[target] << endl;


    return 0;
}
