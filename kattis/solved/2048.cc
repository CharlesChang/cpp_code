#include<bits/stdc++.h>
using namespace std;
int dc[4] = { -1, 0, 1, 0};
int dr[4] = { 0, -1, 0, 1};

void calculate(int move, int table[4][4], int done[4][4], int r, int c){

    int current = table[r][c];
    bool conti = true;
    int nc = c;
    int nr = r;
    while(conti){
        nc += dc[move];
        nr += dr[move];

        //outside
        if(nc == -1 || nc == 4 || nr == -1 || nr == 4) break;

        if(table[nr][nc] == 0){
            swap(table[nr-dr[move]][nc-dc[move]], table[nr][nc]);
        }else if( done[nr][nc] == 0 && current == table[nr][nc] ){ //not done
            table[nr-dr[move]][nc-dc[move]] = 0;
            table[nr][nc] *=2;
            done[nr][nc] = 1;
        }else{
            break;
        }
    }
//    cout << "intermediate " << endl;
//    for(int i=0; i< 4; i++){
//        for(int j=0; j < 4; j++){
//            cout << table[i][j] <<" ";
//        }
//        cout << endl;
//    }
//    cout << endl;
}
int main(){
    int table[4][4];

    for(int i=0; i< 4; i++){
        for(int j=0; j < 4; j++){
            cin >>  table[i][j];
        }
    }

    int move;// 0, 1, 2, or 3 that denotes a left, up, right, or down
    cin >>  move;

    int done[4][4];
    memset(done, 0, sizeof done);

//    for(int i=0; i< 4; i++){
//        for(int j=0; j < 4; j++){
//            cout << table[i][j] <<" ";
//        }
//        cout << endl;
//    }
//    cout << endl;

    if(move < 2){
        for(int r=0; r< 4; r++){
            for(int c=0; c < 4; c++){
                calculate(move, table, done, r, c);
            }
        }
    }else{
        for(int r=3; r >= 0; r--){
            for(int c=3; c >= 0; c--){
                calculate(move, table, done, r, c);
            }
        }
    }
    

    for(int i=0; i< 4; i++){
        for(int j=0; j < 4; j++){
            cout << table[i][j] <<" ";
        }
        cout << endl;
    }
    return 0;
}
