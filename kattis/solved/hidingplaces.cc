//https://open.kattis.com/problems/hidingplaces
#include<bits/stdc++.h>
using namespace std;

int main(){
    int cases;
    cin >> cases;
    int board[10][10];
    int step[10][10];

    int dx[] = {-2,-2,2,2,-1,-1, 1,1};
    int dy[] = {-1,1,-1,1,2,-2,2,-2 };
    while(cases--){
        memset(board, 0, sizeof board);
        memset(step, 0, sizeof step);
        string coor;
        cin >> coor;

        int x,y;
        x = coor[0]-'a';  //must decode reverse Y, otherwise will get wrong answer
        y = 8- (coor[1]-'0');  
        board[y][x] = 1; // set origin as searched
        step[y][x] = 0; // set origin step to be 0

        list<pair<int, int>> que; // x, y 
        que.push_back({y,x});
        int max_dist = 0; //keep a runnng max
        while(!que.empty()){
            pair<int,int> p = que.front();
            que.erase(que.begin());            
            
            //marking
            board[p.first][p.second] = 1;


            for(int i=0; i< 8; i++){
                int ny = p.first + dy[i];
                int nx = p.second +dx[i];

                if(nx >=8 || nx < 0 || ny >= 8 || ny < 0) continue; //out of board

                if( board[ny][nx] != 0 ) continue; // explored

                step[ny][nx] = step[p.first][p.second]+1; //pre-update
                max_dist = max(max_dist, step[ny][nx]); 
                que.push_back({ny, nx});
            }
        }

        cout << max_dist << " ";
        for(int i=0; i< 8; i++){
            for(int j=0; j< 8; j++){
                //cout << step[i][j] << " ";
                if(step[i][j] == max_dist){
                    printf("%c%d ", 'a'+j, 8-i);
                }
            }
            //cout << endl;
        }
        cout << endl;

    }

    return 0;
}

/*
 * Tips: 
 *  1. the y axis is 1 based, convert it to 0 based
 *  2. The y axis runs up, convert it to running up
 */
