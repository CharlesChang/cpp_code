#include <bits/stdc++.h>
using namespace std;

int main(){

    int price, zeros;
    cin >> price >> zeros;

    int mid = price/pow(10,zeros);
    int residue = price - mid*pow(10,zeros);

    if(residue >= 1*pow(10,zeros)/2){
        cout << (int)(price - residue + 1*pow(10,zeros)) << endl;
    }else{
        cout << (int)(price - residue) << endl;
    }
    return 0;
}
