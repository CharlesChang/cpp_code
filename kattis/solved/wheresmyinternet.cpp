#include <bits/stdc++.h>
using namespace std;

typedef vector<vector<int>> vvi;

void dfs(int s, vvi &table, bool record[])
{

  record[s] = true;

  //recurse on all neighbors
  for (auto &i : table[s])
  {
    if (record[i] != true)
      dfs(i, table, record);
  }
}

int main()
{
  int houses, cables;
  cin >> houses >> cables;

  // init AL with rows of empty vector
  vvi AL(houses, vector<int>());

  //init record to vector of not found
  bool record[houses];
  memset(record, false, sizeof record);
  while (cables--)
  {
    int p1, p2;
    cin >> p1 >> p2;

    //convert to 0 based, undirected
    AL[p1 - 1].push_back(p2 - 1);
    AL[p2 - 1].push_back(p1 - 1);
  }

  dfs(0, AL, record);
  bool connected = true;

  for (int i = 0; i < houses; i++)
    if (record[i] == false)
    {
      cout << i + 1 << endl;
      connected = false;
    }

  if (connected)
    cout << "Connected" << endl;
  return 0;
}