#include <bits/stdc++.h>
using namespace std;

int main()
{
  set<pair<string, string>> roll; //< last, first>
  unordered_map<string, int> firstName;
  string first;
  string last;
  while (cin >> first >> last)
  {
    auto itt = firstName.find(first);
    if (itt == firstName.end())
    {
      firstName[first] = 1;
    }
    else
    {
      firstName[first] = itt->second + 1;
    }
    roll.insert({last, first});
  }

  for (auto itt = roll.begin(); itt != roll.end(); itt++)
  {
    if (firstName[itt->second] == 1)
    {
      cout << itt->second << endl;
    }else{
      cout << itt->second << " " << itt->first << endl;
    }
  }

  return 0;
}