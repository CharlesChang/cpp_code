#include <bits/stdc++.h>
using namespace std;

int main()
{
    int stand;
    cin >> stand;
    int arr[110]; // children: parent

    memset(arr, -1, sizeof arr);

    int a;
    int b;
    char c;
    while (scanf("%d ", &a), a != -1)
    {
        while (scanf("%d%c", &b, &c))
        {
            arr[b] = a;

            //break on c is newline char
            if (c != ' ')
                break;
        }
    }

    cout << stand << " ";
    for (int i = arr[stand]; i != -1; i = arr[i])
        cout << i << " ";
    return 0;
}
