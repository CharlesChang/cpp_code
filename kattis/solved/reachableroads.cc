#include<bits/stdc++.h>
using namespace std;

void dfs(int source, int done[], vector<int> AL[]){
    //skip condition
    if(done[source]== 1){
        return;
    }
    done[source] = 1;

    for(auto &i: AL[source]){
        dfs(i, done, AL);
    }
}

int main(){

    int cities;
    cin >> cities;
    while(cities--){
        int endpoints;
        cin >> endpoints;

        int done[endpoints];
        memset(done, 0, sizeof done);

        vector<int>  AL[endpoints];
        fill(AL, AL+endpoints, vector<int>());

        int E;
        cin >> E;
        for(int i=0; i< E; i++){
            int u, v;
            cin >> u >> v;
            AL[u].push_back(v);
            AL[v].push_back(u);
        }

        //start searching for CC
        int count = 0;
        for(int i=0; i< endpoints; i++){
            if(done[i]== 0){
                count++;
                dfs(i, done, AL);
            }
        }
        cout << --count << endl;
    }
   
    return 0;
}
