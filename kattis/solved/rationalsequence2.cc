#include<bits/stdc++.h>
using namespace std;

int main(){

    int N;
    cin >> N;
    
    for(int i=0; i< N; i++){
        int num, f,s;
        scanf("%d %d/%d", &num, &f, &s);
        list<char> path;
        while( !(f ==1 && s == 1) ){
            if( f > s ){ // from right child go parent 
                f = f-s;
                path.push_back('R');
            }else{ // from left child go parent
                s = s-f;
                path.push_back('L');
            }
        }

        int start = 1;
        for(auto i= path.rbegin(); i != path.rend(); i++){
            if(*i == 'R'){
                start  = (start << 1) +1;
            }else{
                start = (start << 1);
            }
        }
        cout << num << " "<< start<< endl;

    }
    return 0;
}
