//https://open.kattis.com/problems/amsterdamdistance
#include<bits/stdc++.h>
using namespace std;

int num_seg, num_halfRing;
double radius;

class point_hash {
    public:
        int theta, r;
        template <class Point>
            size_t operator() (const Point &p) const {
                return hash<int>()(p.theta) ^ hash<int>()(p.r) ^ hash<int>()(p.theta) ^ hash<int>()(p.r);
            }
};

class Point{
    public:
        int theta, r;
        Point(int theta, int r){
            this->theta = theta;
            this->r = r;
        }
        Point(){}

        double distanceTo(Point &p){
            double seg_len = radius/num_halfRing;
            if(p.r != r){
                //the 2 points differ in r direction
                return seg_len * (abs(p.r - r));
            }else{
                //the 2 points differ in theta direction
                return (seg_len*this->r) * ( abs(this->theta - p.theta) * (M_PI/num_seg));
            }
        }

        //required to override == operator for the unordered_map 
        bool operator== (const Point &p) const {
            return theta == p.theta && r == p.r;
        }
        //comparison function, overload relational operator, this is a must for set<>
        bool operator< (const Point &p) const{
            return theta < p.theta ;
        }

        string toString(){
            return  "{" + to_string(this->theta) +","+ to_string(this->r) +"}";
        }
};

class Solution{
    public:
        double dijstras(int a_theta, int a_r, int b_theta, int b_r){

            Point source(a_theta,a_r);
            Point target(b_theta,b_r);
            //start dijstras
            multiset<pair<double, Point>> que; //<dist, Point 
            que.insert({0, source});

            unordered_map<Point, double, point_hash> dist; //Point , dist
            dist[source] = 0; //set starting point to have dist 0

            int d_theta[4] = {1,-1,0,0};
            int d_r[4] = {0,0,1,-1};


            while(!que.empty()){
                Point u = que.begin()->second;
                double ed = que.begin()->first;
                que.erase(que.begin());

                //cout <<"onto " <<u.toString() << endl;
                //skip condition
                if(dist[u] < ed){
                    continue;
                }

                //termination condition
                if(u == target){
                    return ed;
                }

                //4 neighbors
                for(int i=0; i< 4; i++){
                    int n_theta = u.theta + d_theta[i];
                    int n_r = u.r + d_r[i];

                    //check out of grid case
                    if(n_r < 0 || n_r > num_halfRing || n_theta < 0 || n_theta > num_seg) continue;

                    Point v(n_theta, n_r);
                    //try to relax
                    if( dist.find(v) == dist.end() ||  ed+ u.distanceTo(v) < dist[v] ){
                        dist[v] = ed+ u.distanceTo(v);
                        que.insert({dist[v], v});
                    }

                }
            }
            return -1;
        }

        double greedy(int a_theta, int a_r, int b_theta, int b_r){
            double seg_len = radius/num_halfRing;
            double minDist = 2e9;

            int low = min(a_r, b_r);
            for(int i = low; i >= 0; i--){
                double line1= this->lineDifference(a_r,i,seg_len);    
                double line2= this->lineDifference(b_r,i,seg_len);    
                double arch= this->archDifference(a_theta, b_theta, seg_len, i );
                //cout << line1 + line2 + arch << " ";
                minDist = min(minDist, line1+line2+arch);
            }

            return minDist;
        }

    private:
        double lineDifference(int a,int b, double seg_len){
            return abs(a-b)*seg_len;
        }

        double archDifference(int a, int b, double seg_len , int n){
            return (seg_len * n) * (M_PI/num_seg) * (abs(a-b));
        }
};
int main(){

    Solution s;
    int a_theta,a_r,b_theta,b_r;
    while(cin >> num_seg >> num_halfRing >> radius, cin >> a_theta >> a_r >> b_theta >> b_r){
        printf("%f\n", s.greedy(a_theta,a_r,b_theta,b_r));
        //printf("%f\n", s.dijstras(a_theta,a_r,b_theta,b_r));

    }


    return 0;
}

/*
 * Notes
 * 1. Hashing function for unordered_map example
 * 2. Take note how to write lambda function
 *      [capture list](param){body}
 * 3. See how to overload operators
 */
