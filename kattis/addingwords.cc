//https://open.kattis.com/problems/addingwords
#include<bits/stdc++.h>
using namespace std;
typedef unordered_map<string, int> umsi;
void printStream(vector<string> &st){
    for(string &s : st){
        cout << s << " ";
    }
    return;
}
void exec(int flag, vector<string> &st, umsi &defi){
//    for(string &s : st){
//        cout << s;
//    }
    cout << endl;
    if( flag == 0 ){
        // defining
        defi[st[0]] = stoi(st[1]);
    }else{
        // calculating        
        int unknownFlag  = false;
        auto first = defi.find(st[0]);
        if( first == defi.end() ){
            printStream(st); 
            cout << "unknown"<<endl;
            return;
        }
        
        int f = first->second; //get the first number

        for(int i=0; i< st.size(); i+=2){
            string operation =  st[i];
            if( operation == "=" ){
                printStream(st);
                cout << f << endl;
            }
        } 
        cout << endl;
    }
}

int main(){
    char n[40];
    char x;

    int flag = -1; // 0 = def, 1 = calc
    vector<string> st;
    umsi defi;
    while(scanf("%s%c", &n[0], &x)){
        //scan the first word
        if(string(n) == "clear") break;

        
        if(flag == 0 || flag == 1){
            st.emplace_back(string(n));
        }else{

            if(string(n) == "def"){
                flag = 0;
            }else if(string(n) == "calc"){
                flag = 1;
            }
        }
        // start next line
        if(x != ' '){
            exec(flag, st, defi);
            st.clear();
            flag = -1;
        }
    }
    return 0;
}
