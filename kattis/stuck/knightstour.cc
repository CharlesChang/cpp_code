#include<bits/stdc++.h>
using namespace std;
const int N = 8;
class Solution{
    public:
        bool solve(int table[N][N], int seq[N*N+1]){
            int startx = -1, starty = -1;

            //find the starting point
            for(int i=0; i< N; i++){
                for(int j=0; j< N; j++){
                    if(table[i][j] == 1){
                        startx = i;
                        starty = j;
                        break;
                    }
                }
                if(startx != -1) break;
            }

            //start the algorithm
            if(search(table,startx,starty,2, seq) == false){
                cout << "No Solution" << endl;
                return false;
            }else{
                printTable(table);
            }
            return true;
        }


        void printTable(int table[N][N]){
            for(int i=0; i< N; i++){
                for(int j=0; j< N; j++){
                    printf(" %2d ", table[i][j]);
                }
                cout << endl;
            }
        }
    private:
        int dx[8] = { -1,1,-2,2,-2,2,-1,1};
        int dy[8] = { -2,-2,-1,-1,1,1,2,2};

        /*
         * The utility function to check is it is safe to proceed
         */
        bool isSafe(int x,int y, int table[N][N]){
           return  ( x >= 0 && x < N && y >= 0 &&y < N && table[x][y] == -1);       
            
        }
        bool isPrescirbedMove(int x, int y, int table[N][N], int step){
           return  ( x >= 0 && x < N && y >= 0 &&y < N && table[x][y] == step);       
        }

        bool search(int table[N][N], int current_x, int current_y, int step, int seq[N*N+1]){
            if(step == N*N+1){
                return true;
            }
            //recursively try all moves
            for(int k=0; k< 8; k++){

                int nx = current_x + this->dx[k];
                int ny = current_y + this->dy[k];
                
                if(seq[step] == 1){ //next step is prescribed
                    //able to find next step
                    if(this->isPrescirbedMove(nx,ny,table,step) == false){ 
                        continue;
                    }
                }else{ //next step is a free step
                    if(this->isSafe(nx,ny,table) ==  false){
                        continue;
                    }
                }
                table[nx][ny] = step; //mark the next step

                //recursively try next move
                if(search(table, nx, ny, step+1, seq) == true){
                    return true;
                }else{
                    //backtrack
                    if(seq[step] == 0){
                        //not prescribed step, we retract it
                        table[nx][ny] = -1;
                    }
                }
            }
            return false;
        }

};

int main(){
    Solution s;
    int table[N][N];
    int seq[N*N+1];
    memset(seq, 0, sizeof seq);
    for(int i=0; i< N; i++){
        for(int j=0; j< N; j++){
            int num;
            cin >> num;
            if(num != -1){
                seq[num] = 1; //record existing number
            }
            table[i][j] = num;
        }
    }
    //s.printTable(table);
    //cout << endl;
    s.solve(table, seq);
    //cout << endl << "After operation" << endl;
    //s.printTable(table);
    return 0;
}
