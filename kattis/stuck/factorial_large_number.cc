//https://open.kattis.com/problems/inversefactorial
#include<bits/stdc++.h>
using namespace std;
using ull = unsigned long long;


class FacNumber{
    public:
        void multiply(ull x){
            int carry = 0;
            int len = this->res.size();
            for(int i = 0; i< len; i++){
                int prod = this->res[i] * x + carry;
                
                this->res[i] = prod%10;
                carry = prod/10;
            }    
            while (carry)
            {
                res.emplace_back(carry%10);
                carry /= 10;
            }
        }

        /*
         * Construct facNumber 0
         */
        FacNumber(){
            this->res.push_back(0);
        }

        /*
         * Construct facNumber equal to x
         */
        FacNumber(ull x){
            while(x){
                this->res.push_back(x%10);
                x = x/10;
            }
        }

        /*
         * Construct FacNumer from a string
         */
        FacNumber(string &s){
            this->res.clear();
            for(int i= s.size()-1; i >= 0; i--){

                //convert char to int
                this->res.emplace_back(  (s[i]- '0')  );
            }
        }

        /*
         * Sec this facNumber to the factorial(x)
         */
        void setToFac(int x){
            this->res.clear();
            if(x == 0 || x == 1){
                this->res.push_back(0);
                return;
            }

            this->res.push_back(1); 
            for(int i = 2; i <= x; i++){
                this->multiply(i);
            }
        }


        /* 
         * Print out the current factorial number
         */
        void out(){
            for(int i = this->res.size()-1; i >= 0; i--){
                cout << this->res[i];
            }
            cout << endl;
        }
        void out() const {
            for(int i = this->res.size()-1; i >= 0; i--){
                cout << this->res[i];
            }
            cout << endl;
        }

        /*
         * Find n from n!
         */
        int reverse(){
            if( this->res[0] == 1){
                return 1;
            }

            int next = 1;
            FacNumber f(1);
            while(*this != f){
                next++;
                f.multiply(next);
                //cout << next << " ";
            } 

            return next;
        }



        /*
         * Overloading binary operator inside class takes only 1 param since 
         * the other is implicitly "this"
         */
        bool operator== (const FacNumber &f2) {
            //cout << "--> {"<< this->res.size() << " , " << f2.res.size() << "} ";
            if(this->res.size() != f2.res.size()){
                return false;
            }
            if( this->res[0] != f2.res[0] ){
                return false;
            }

            return true;
        }

        bool operator !=(const FacNumber &f){
            return  !(*this == f);
        }

    private:
        vector<int> res;

};

int main(){

    string s;
    while(cin >> s){
        FacNumber f(s);
        cout << f.reverse() << endl;
    }

    return 0;
}
