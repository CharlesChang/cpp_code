#include<bits/stdc++.h>
using namespace std;
class Item{
    public:
        int *pt;
        int val;
        //Item(int *pt, int val){
        //    this->pt = pt;
        //    this->val = val;
        //}
};
int main(){

    float C;
    int n;

    while(cin >> C >> n){

        //reading weight value pair
        vector<tuple<int, int, int>> store;
        for(int i=0; i< n; i++){
            int wt, val;
            cin >> val>> wt;
            store.push_back(make_tuple(wt, val, i));
        }
        sort(store.begin(), store.end());
        
        //solution table
        int table[n+1][(int)C+1];
        memset(table, 0, sizeof table);
        
        Item chosen[n+1][(int)C+1];


        //finding solution
        for(auto lvlWeight = 0 ; lvlWeight  <= n; lvlWeight++){
            for(int cap = 0; cap <= C; cap++){
                if(cap == 0  || lvlWeight == 0){
                    table[lvlWeight][cap] = 0;
                }

                //cannot include
                else if(cap < get<0>(store[lvlWeight-1])  ){
                    table[lvlWeight][cap] = table[lvlWeight-1][cap];
                    chosen[lvlWeight][cap].pt =  &chosen[lvlWeight-1][cap];
                }
                
                else{
                    //max of include and don't include
                    int wt = get<0>(store[lvlWeight-1]);
                    int val = get<1>(store[lvlWeight-1]);

                    int include = val + table[lvlWeight-1][cap-wt];
                    int dontInclude = table[lvlWeight-1][cap];

                    if(include > dontInclude){
                        table[lvlWeight][cap] = include;
                        chosen[lvlWeight][cap] = chosen[lvlWeight-1][cap-wt];
                        chosen[lvlWeight][cap].push_back(get<2>(store[lvlWeight-1]));
                    }else{
                        table[lvlWeight][cap] = dontInclude;
                        chosen[lvlWeight][cap].pt =  &chosen[lvlWeight-1][cap];
                    }


                }

            }
        }

        //for(int i=0; i<= n; i++){
        //    for(int j=0; j<= (int)C; j++){
        //        cout << table[i][j] << " ";
        //    }
        //    cout << endl;
        //}
        //cout << table[n][(int)C] << endl;

        //count items and print answers
        cout << chosen[n][(int)C].size() << endl;

        
        for(int i = chosen[n][(int)C].size()-1; i >= 0; i--){
            cout <<  chosen[n][(int)C][i] << " ";
        }
        cout << endl;
    }

    return 0;
}
